#!/bin/bash

#create path to redirect accounts.csv to the same directory as accounts_new.csv
dir_path=$(dirname "$1")

# Substitute commas with vertical lines, so the sed command works
awk -F'"' -v OFS='"' '{ for (i=2; i<=NF; i+=2) gsub(",", "|", $i) } 1' accounts.csv |

awk -F',' -v OFS=',' '
BEGIN { FS="\""; OFS="," }                              # input is delimited by double quotes
NR==1 { print; next }
      { line=""
        for (i=1;i<NF;i+=2) {                           # loop through odd-numbered fields
            gsub(/,/,"|",$(i+1))                        # in even-numbered double-quote-delimited fields, replace commas with pipes
            line=line $i FS $(i+1) FS                   # rebuild the current line
        }
        line=line $NF                                   # add the last field to the new line

        split(line,a,",")                               # split the new line on commas
        split(tolower(a[3]),b,/[[:space:]-]+/)           # split tolower(name field) on whitespace and hyphen

        # rebuild name with first characters of first/last names uppercased
        name=""
        for (i=1; i<=length(b); i++) {
            if (b[i] != "") {
                if (i > 1 && i < length(b)) {
                    name = name toupper(substr(b[i],1,1)) tolower(substr(b[i],2)) "-"
                } else {
                    name = name toupper(substr(b[i],1,1)) tolower(substr(b[i],2)) " "
                }
            }
        }
        name = substr(name, 1, length(name)-1)           # remove the trailing space

        acct=substr(b[1],1,1) b[2]                      # build email account name

        if (acct == "mbaker") {
            acct = "mbaker-venturini"
        }

        lines[NR]=a[1] OFS a[2] OFS name OFS a[4]       # rebuild current line based on the first 4 fields
        locid[NR]=a[2]                                  # make a note of the location_id for the current line
        dept[NR]=a[6]  
        email[NR]=acct                                  # make a note of the email account for the current line
        count[acct]++                                   # keep count of the number of times we see this email account
      }

END   { for (i=2;i<=NR;i++) {                           # loop through our lines of output
            gsub(/\|/,",",lines[i])                   # replace pipes with original commas

            # print the final line of output; if the email account has been seen more than once, then append the location_id to the email account; add the "@abc.com" domain and the trailing comma
            print(lines[i] OFS email[i] (count[email[i]] > 1 ? locid[i] : "") "@abc.com" OFS dept[i])
        }
      }
' $1 > $dir_path"/accounts_new.csv"

echo "Done. '$dir_path/accounts_new.csv'. Success"
